﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AudioController : MonoBehaviour
{
    public Slider volume;
    private float audioVolume;
    public Text text;

    // Start is called before the first frame update
    void Start()
    {
        audioVolume = PlayerPrefs.GetFloat("Volume");
        volume.value = audioVolume * volume.maxValue;
        AudioListener.volume = audioVolume;
        text.text = (audioVolume * volume.maxValue).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        audioVolume = volume.value / volume.maxValue;
        AudioListener.volume = audioVolume;
        text.text = (audioVolume * volume.maxValue).ToString("F0");
    }

    public void VolumePrefs()
    {
        PlayerPrefs.SetFloat("Volume", audioVolume);
    }
}
