﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ColliderCheck();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ColliderCheck()
    {
        int WorkingTowersNum = 0;
        Tower[] towerList = GetComponentsInChildren<Tower>();
        foreach (Tower go in towerList)
        {
            if (go.towerData.isFunctional)
            {
                WorkingTowersNum++;
            }
        }
        if (WorkingTowersNum <= 0)
        {
            // turn collider for ship on
            GetComponent<PolygonCollider2D>().enabled = true;
        }
        else
        {
            GetComponent<PolygonCollider2D>().enabled = false;
        }
    }
}
