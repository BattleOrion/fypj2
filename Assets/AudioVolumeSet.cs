﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioVolumeSet : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.GetInt("FirstTimeInit", 1) == 1)
        {
            PlayerPrefs.SetFloat("Volume", 1);
            AudioListener.volume = PlayerPrefs.GetFloat("Volume");
            PlayerPrefs.SetInt("Gore", 1);
            PlayerPrefs.SetInt("FirstTimeInit", 0);
        }
        else
        {
            AudioListener.volume = PlayerPrefs.GetFloat("Volume");
        }
    }
}