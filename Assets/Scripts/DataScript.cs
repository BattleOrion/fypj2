﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataScript : MonoBehaviour
{
    public int Cur_Health;
    public int Max_Health;

    public bool isFunctional;
    public int energyCost;
    private bool isPowered;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool canBePowered()
    {
        if (energyCost > 0)
        {
            return true;
        }
        return false;
    }
    
    public bool getPowered()
    {
        return isPowered;
    }

    public void setPowered(bool input)
    {
        isPowered = input;
    }

    public bool CheckFunctional()
    {
        return isFunctional;
    }

    public void TakeDamage(int damage)
    {
        if (Cur_Health - damage <= 0)
        {
            Cur_Health = 0;
            isFunctional = false;
            GetComponent<CircleCollider2D>().enabled = false;
        }
        else if (Cur_Health - damage > 0)
        {
            Cur_Health -= damage;
        }
    }

    public void SetFunctional()
    {
        if (Cur_Health > 0)
        {
            isFunctional = true;
            GetComponent<CircleCollider2D>().enabled = true;
        }
        else
        {
            isFunctional = false;
            GetComponent<CircleCollider2D>().enabled = false;
        }
    }

    public bool getFunctional_Powered()
    {
        return (isPowered && isFunctional);
    }
}
