﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyScript : MonoBehaviour
{
    public int energyValue;
    public Sprite onlineSprite, offlineSprite;
    private bool isOnline;

    // Start is called before the first frame update
    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetOnline(bool input)
    {
        isOnline = input;
        if (input)
        {
            GetComponent<SpriteRenderer>().sprite = onlineSprite;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = offlineSprite;
        }
    }

    public bool GetOnline()
    {
        return isOnline;
    }
}
