﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    public void SwitchScene(int scene)
    {
        if (scene == 2)
        {
            AudioManager.instance.PlayMusic(AudioManager.SOUND_EFFECT.InGame);
        }
        else
        {
            AudioManager.instance.PlayMusic(AudioManager.SOUND_EFFECT.Menu);
        }

        SceneManager.LoadScene(scene);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
