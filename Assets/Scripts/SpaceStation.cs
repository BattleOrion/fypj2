﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceStation : MonoBehaviour
{
    public bool containsTower = true;
    public Tower[] Towers;
    public Tower myTower;

    // Start is called before the first frame update
    void Start()
    {
        containsTower = true;
        Tower newTower = Instantiate(Towers[0]);
        newTower.transform.position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        GameObject.Destroy(myTower);
    }
}
