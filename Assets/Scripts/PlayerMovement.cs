﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;

    public float velocityMultiplier, rotationMultiplier, maxVelocity, maxAngularVelocity;
    public float linearDrag, angularDrag;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float moveForce = Input.GetAxis("Vertical") * velocityMultiplier;
        float rotateForce = Input.GetAxis("Horizontal") * rotationMultiplier;

        ThrustForward(moveForce);
        Rotate(-rotateForce);   
    }

    void ThrustForward(float amount)
    {
        Vector2 force = transform.up * amount;

        rb.AddForce(force);
        ClampVelocity();
    }

    void Rotate(float amount)
    {
        rb.AddTorque(amount);
        ClampRotation();
    }

    void ClampVelocity()
    {
        float x = Mathf.Clamp(rb.velocity.x, -maxVelocity, maxVelocity);
        float y = Mathf.Clamp(rb.velocity.y, -maxVelocity, maxVelocity);

        rb.velocity = new Vector2(x, y);
    }

    void ClampRotation()
    {
        float r = Mathf.Clamp(rb.angularVelocity, -maxAngularVelocity, maxAngularVelocity);

        rb.angularVelocity = r;
    }

    public void SetRigidBody(Rigidbody2D rigidbody)
    {
        rb = rigidbody;
    }
}
