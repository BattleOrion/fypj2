﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteriod : MonoBehaviour
{
    public int index;

    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Vector2 force = transform.up * (Random.Range(0, 9) * 10);
        rb.AddForce(force);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
