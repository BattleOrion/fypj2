﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveProjectileScript : MonoBehaviour
{
    [System.Serializable]
    public class ExplosionData
    {
        public GameObject objType;
        public float damageRatio;
    }
    
    public float distanceLimit;
    public List<ExplosionData> explosionList;

    public Sprite originSprite;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        gameObject.GetComponent<Rigidbody2D>().simulated = true;
        gameObject.GetComponent<CapsuleCollider2D>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        //if (!ObjectPooler.SharedInstance.CheckWithinBounds(gameObject.transform.position))
        //{
        //    gameObject.SetActive(false);
        //}
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        bool onHit = false;

        foreach (ExplosionData type in explosionList)
        {
            if (other.gameObject.CompareTag(type.objType.tag))
            {
                ExplodeDamage(other.gameObject);
                if (!onHit)
                {
                    onHit = true;
                }
            }
        }

        if (onHit)
        {
            gameObject.GetComponent<Rigidbody2D>().simulated = false;
            gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            gameObject.GetComponent<Animator>().SetBool("Exploded", true);
        }
    }

    public void ExplodeDamage(GameObject input)
    {
        Object[] all_rigidbodies = FindObjectsOfType(typeof(Rigidbody2D));

        foreach (Rigidbody2D r in all_rigidbodies)
        {
            if (Vector2.Distance(r.transform.position, transform.position) < distanceLimit)
            {
                foreach (ExplosionData type in explosionList)
                {
                    if (r.gameObject.CompareTag(type.objType.tag))
                    {
                        // deal damage here
                        r.gameObject.SetActive(false);
                        Debug.Log("bullet g0ne");
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, distanceLimit);
    }
}
