﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectileScript : MonoBehaviour
{
    float timer;
    float TimerSet = 7.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        timer = TimerSet;
    }

    // Update is called once per frame
    void Update()
    {
        //if (!ObjectPooler.SharedInstance.CheckWithinBounds(gameObject.transform.position))
        //{
        //    gameObject.SetActive(false);
        //}
        timer -= Time.deltaTime;
        if (timer <= 0.0f)
        {
            timer = TimerSet;
            gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (gameObject.CompareTag("Enemy_Bullet"))
        {
            if (other.collider.gameObject.CompareTag("Player_Tower"))
            {
                // deal damage her
                //other.collider.gameObject.CompareTag
                other.collider.gameObject.GetComponent<Tower>().DealDamage(1);
                gameObject.SetActive(false);
            }
            else if (other.collider.gameObject.CompareTag("Player_Barrier"))
            {
                gameObject.SetActive(false);
                BarrierScript obj = other.collider.gameObject.GetComponentInParent<BarrierScript>();
                obj.TakeDamage(1);
            }
            else if (other.collider.gameObject.CompareTag("Player_Ship"))
            {
                // deal damage her
                other.collider.gameObject.GetComponent<Player>().TakeDamage(1);               

                gameObject.SetActive(false);
            }
            else if (other.collider.gameObject.CompareTag("Asteroid"))
            {
                // deal damage her
                other.collider.gameObject.SetActive(false);

                gameObject.SetActive(false);
            }
        }
        else if (gameObject.CompareTag("Player_Bullet"))
        {
            if (other.collider.gameObject.CompareTag("Enemy_Ship"))
            {
                // deal damage her
                other.collider.gameObject.GetComponent<EnemyAI>().DealDamage(1);
                gameObject.SetActive(false);
            }
            else if (other.collider.gameObject.CompareTag("Asteroid"))
            {
                // deal damage her
                other.collider.gameObject.SetActive(false);

                gameObject.SetActive(false);
            }
        }
    }
}
