﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierScript : MonoBehaviour
{
    public int energyCost;
    public float CurrBarrierHP;
    public float MaxBarrierHP;
    public float HPChange;

    public GameObject BarrierObj;
    private bool isHit;
    private float hitTimer;
    public bool rechargeBool;
    public bool ReadyToCharge;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        CurrBarrierHP = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<DataScript>().getFunctional_Powered())
        {
            if (HPChange != CurrBarrierHP)
            {
                HPChange = CurrBarrierHP;
                BarrierObj.transform.localScale = new Vector3(HPChange + 1, HPChange + 1, 1);
            }
            if (isHit)
            {
                //reduce the recharge time
                if (hitTimer > 0.0f)
                {
                    hitTimer -= Time.deltaTime;
                }
                if (hitTimer < 0.0f)
                {
                    isHit = false;
                    rechargeBool = true;
                }
            }
            if (rechargeBool)
            {
                if (CurrBarrierHP < MaxBarrierHP)
                {
                    CurrBarrierHP += 2.0f * Time.deltaTime;
                }
                else
                {
                    CurrBarrierHP = MaxBarrierHP;
                    rechargeBool = false;
                }
            }
        }
        else
        {
            if (CurrBarrierHP > 0.0f)
            {
                CurrBarrierHP = HPChange = 0.0f;
                rechargeBool = true;
                BarrierObj.transform.localScale = new Vector3(HPChange + 1, HPChange + 1, 1);
            }
        }
    }

    public void TakeDamage(float damage)
    {
        if (CurrBarrierHP > 0.0f)
        {
            CurrBarrierHP -= damage;
            if (CurrBarrierHP <= 0.0f)
            {
                CurrBarrierHP = 0.0f;
            }
            isHit = true;
            hitTimer = 1.0f;
            rechargeBool = false;
        }

        if (CurrBarrierHP - damage > 0)
        {
            CurrBarrierHP -= damage;
            isHit = true;
            hitTimer = 1.0f;
            rechargeBool = false;
        }
        else if ((CurrBarrierHP - damage < 0) && (isHit == true))
        {
            CurrBarrierHP = 0.0f;
        }
        else if ((CurrBarrierHP - damage < 0) && (isHit == false))
        {
            CurrBarrierHP = 0.0f;
            isHit = true;
            hitTimer = 1.0f;
            rechargeBool = false;
        }
    }

    public void DisableBarrier()
    {
        HPChange = CurrBarrierHP = 0;
        BarrierObj.transform.localScale = new Vector3(HPChange + 1, HPChange + 1, 1);
        BarrierObj.GetComponent<CircleCollider2D>().enabled = false;
        //rechargeBool = true;
        ReadyToCharge = true;
    }
}
