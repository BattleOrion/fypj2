﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScrolling : MonoBehaviour
{
    public Player player;
    public GameObject sampleBG;
    public float scrollSpeed;
    public GameObject[] backgrounds = new GameObject[9];

    private Vector3 sizeBG;
    private Vector3 currPlayerPos;
    private Vector3 lastPlayerPos;
    private GameObject currentPlayerContainer;
    
    void Start()
    {
        sizeBG = sampleBG.GetComponent<Renderer>().bounds.size * transform.localScale.x;
        currPlayerPos = lastPlayerPos = player.transform.position;
        currentPlayerContainer = backgrounds[4];
    }
    
    void Update()
    {
        ScrollBackground();
        ShiftBackground();
    }

    void ShiftBackground()
    {
        if (player.transform.position.y > currentPlayerContainer.transform.position.y + sizeBG.y * 0.5f)
        {
            ShiftUp();
        }
        else if (player.transform.position.y < currentPlayerContainer.transform.position.y - sizeBG.y * 0.5f)
        {
            ShiftDown();
        }

        if (player.transform.position.x > currentPlayerContainer.transform.position.x + sizeBG.x * 0.5f)
        {
            ShiftRight();
        }
        else if (player.transform.position.x < currentPlayerContainer.transform.position.x - sizeBG.x * 0.5f)
        {
            ShiftLeft();
        }
    }

    void ScrollBackground()
    {
        currPlayerPos = player.transform.position;
        Vector3 playerDelta = currPlayerPos - lastPlayerPos;
        for (int i = 0; i < 9; ++i)
        {
            backgrounds[i].transform.position += playerDelta * scrollSpeed;
        }
        lastPlayerPos = currPlayerPos;
    }

    void ShiftLeft()
    {
        backgrounds[6].transform.position += Vector3.left * (sizeBG.x * 3);
        backgrounds[7].transform.position += Vector3.left * (sizeBG.x * 3);
        backgrounds[8].transform.position += Vector3.left * (sizeBG.x * 3);

        GameObject[] temp = new GameObject[9];
        temp[0] = backgrounds[6];
        temp[1] = backgrounds[7];
        temp[2] = backgrounds[8];

        temp[3] = backgrounds[0];
        temp[4] = backgrounds[1];
        temp[5] = backgrounds[2];

        temp[6] = backgrounds[3];
        temp[7] = backgrounds[4];
        temp[8] = backgrounds[5];

        backgrounds = temp;
        currentPlayerContainer = backgrounds[4];
    }

    void ShiftRight()
    {
        backgrounds[0].transform.position += Vector3.right * (sizeBG.x * 3);
        backgrounds[1].transform.position += Vector3.right * (sizeBG.x * 3);
        backgrounds[2].transform.position += Vector3.right * (sizeBG.x * 3);

        GameObject[] temp = new GameObject[9];
        temp[0] = backgrounds[3];
        temp[1] = backgrounds[4];
        temp[2] = backgrounds[5];
    
        temp[3] = backgrounds[6];
        temp[4] = backgrounds[7];
        temp[5] = backgrounds[8];
    
        temp[6] = backgrounds[0];
        temp[7] = backgrounds[1];
        temp[8] = backgrounds[2];

        backgrounds = temp;
        currentPlayerContainer = backgrounds[4];
    }

    void ShiftUp()
    {
        backgrounds[5].transform.position += Vector3.up * (sizeBG.y * 3);
        backgrounds[2].transform.position += Vector3.up * (sizeBG.y * 3);
        backgrounds[8].transform.position += Vector3.up * (sizeBG.y * 3);

        GameObject[] temp = new GameObject[9];
        temp[0] = backgrounds[2];
        temp[1] = backgrounds[0];
        temp[2] = backgrounds[1];

        temp[3] = backgrounds[5];
        temp[4] = backgrounds[3];
        temp[5] = backgrounds[4];

        temp[6] = backgrounds[8];
        temp[7] = backgrounds[6];
        temp[8] = backgrounds[7];

        backgrounds = temp;
        currentPlayerContainer = backgrounds[4];
    }

    void ShiftDown()
    {
        backgrounds[0].transform.position += Vector3.down * (sizeBG.y * 3);
        backgrounds[3].transform.position += Vector3.down * (sizeBG.y * 3);
        backgrounds[6].transform.position += Vector3.down * (sizeBG.y * 3);

        GameObject[] temp = new GameObject[9];
        temp[0] = backgrounds[1];
        temp[1] = backgrounds[2];
        temp[2] = backgrounds[0];

        temp[3] = backgrounds[4];
        temp[4] = backgrounds[5];
        temp[5] = backgrounds[3];

        temp[6] = backgrounds[7];
        temp[7] = backgrounds[8];
        temp[8] = backgrounds[6];

        backgrounds = temp;
        currentPlayerContainer = backgrounds[4];
    }
}
