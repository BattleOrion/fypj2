﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurretScript : MonoBehaviour
{
    [SerializeField]
    private GameObject turretBarrel, turretProjectilePrefab, turretProjectileGen;

    public Player player;
    public float turretFirerate;
    public float BulletForce;

    private GameObject NewProjectile;
    private float firerateCooldown;


    // Start is called before the first frame update
    void Start()
    {
        player = transform.parent.GetComponent<EnemyAI>().player;
        firerateCooldown = turretFirerate;
    }

    // Update is called once per frame
    void Update()
    {
        LookAtTarget();

        firerateCooldown -= Time.deltaTime;

        if (firerateCooldown <= 0)
         {
            if ((player.transform.position - gameObject.transform.position).magnitude < 15.0f)
            {
                AudioManager.instance.PlaySound(AudioManager.SOUND_EFFECT.Shoot);
                firerateCooldown = turretFirerate;
                FireProjectile();
            }
        }        
    }

    private void LookAtTarget()
    {
        float AngleRad = Mathf.Atan2(player.transform.position.y - turretBarrel.transform.position.y, player.transform.position.x - turretBarrel.transform.position.x);
        float AngleDeg = Mathf.Rad2Deg * AngleRad;

        turretBarrel.transform.eulerAngles = new Vector3(turretBarrel.transform.eulerAngles.x, turretBarrel.transform.eulerAngles.y, AngleDeg);
    }

    private void FireProjectile()
    {
        NewProjectile = ObjectPooler.SharedInstance.GetPooledObject(turretProjectilePrefab.tag);
        if (NewProjectile != null)
        {
            Vector2 directionVector = new Vector2(player.transform.position.x - turretBarrel.transform.position.x, player.transform.position.y - turretBarrel.transform.position.y);
            float rotationalAngle = (Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg);
            NewProjectile.transform.position = turretProjectileGen.transform.position;
            NewProjectile.transform.rotation = Quaternion.AngleAxis(rotationalAngle, Vector3.forward);
            NewProjectile.SetActive(true);
            NewProjectile.GetComponent<Rigidbody2D>().AddForce(directionVector.normalized * BulletForce);
        }
    }
}
