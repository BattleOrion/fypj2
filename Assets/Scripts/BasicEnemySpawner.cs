﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemySpawner : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public int maxActiveEnemy;
    public float spawnTime;
    private float spawnCountdown;

    private Vector2 minVector, maxVector;

    // Start is called before the first frame update
    void Start()
    {
        minVector = ObjectPooler.SharedInstance.GetMinVector();
        maxVector = ObjectPooler.SharedInstance.GetMaxVector();
    }

    // Update is called once per frame
    void Update()
    {
        if (ObjectPooler.SharedInstance.GetActiveTag("Enemy") < maxActiveEnemy)
        {
            spawnCountdown -= Time.deltaTime;
            if (spawnCountdown <= 0.0f)
            {
                spawnCountdown = spawnTime;
                float randX = Random.Range(minVector.x, maxVector.x);
                float randY = Random.Range(minVector.y, maxVector.y);

                GameObject createdEnemy = ObjectPooler.SharedInstance.GetPooledObject(EnemyPrefab.tag);
                createdEnemy.transform.position = new Vector2(randX, randY);
                createdEnemy.SetActive(true);
            }
        }
    }
}
