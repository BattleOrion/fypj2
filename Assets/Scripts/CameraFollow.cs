﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;
    public float smoothSpeed = 0.125f;
    private Vector3 offset;

    public bool smoothCamera;
    public float initalZoom, minZoom, maxZoom;

    void Start()
    {
        offset = transform.position - player.transform.position;
        GetComponent<Camera>().orthographicSize = initalZoom;
    }

    void FixedUpdate()
    {
        if (smoothCamera)
        {
            Vector3 desiredPostion = player.transform.position + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPostion, smoothSpeed);
            transform.position = smoothedPosition;
        }

        //Scroll wheel to ajust cam size
        GetComponent<Camera>().orthographicSize -= Input.mouseScrollDelta.y;
        //Clap size between min and max values
        GetComponent<Camera>().orthographicSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize, minZoom, maxZoom);
    }

    void LateUpdate()
    {
        if (!smoothCamera)
        {
            transform.position = player.transform.position + offset;
        }
    }
}
