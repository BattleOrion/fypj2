﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTurretScript : MonoBehaviour
{
    [SerializeField]
    private GameObject turretBarrel, turretProjectilePrefab, turretProjectileGen;

    public Sprite ActiveTurret, OfflineTurret;
    public LineRenderer laser;

    public float firerateCooldown, turretFirerate;
    private Vector2 worldpos;
    public float BulletForce;

    private GameObject NewProjectile;

    public bool canFire;

    // Start is called before the first frame update
    void Start()
    {
        SetTurretSprite(GetComponent<DataScript>().CheckFunctional());
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<DataScript>().getFunctional_Powered())
        {
            if (canFire)
            {
                LookAtTarget();

                firerateCooldown -= Time.deltaTime;
                if (Input.GetMouseButtonDown(0))
                {                
                    if (firerateCooldown <= 0)
                    {
                        AudioManager.instance.PlaySound(AudioManager.SOUND_EFFECT.Shoot);
                        firerateCooldown = turretFirerate;
                        FireProjectile();
                    }
                }
            }
        }
    }

    private void LookAtTarget()
    {
        worldpos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));

        float AngleRad = Mathf.Atan2(worldpos.y - turretBarrel.transform.position.y, worldpos.x - turretBarrel.transform.position.x);
        float AngleDeg = Mathf.Rad2Deg * AngleRad;

        turretBarrel.transform.eulerAngles = new Vector3(turretBarrel.transform.eulerAngles.x, turretBarrel.transform.eulerAngles.y, AngleDeg);   
        //turretBarrel.GetComponent<Rigidbody2D>().MoveRotation(AngleDeg);
    }

    private void FireProjectile()
    {
        NewProjectile = ObjectPooler.SharedInstance.GetPooledObject(turretProjectilePrefab.tag);
        if (NewProjectile != null)
        {
            Vector2 directionVector = new Vector2(worldpos.x - turretBarrel.transform.position.x, worldpos.y - turretBarrel.transform.position.y);
            float rotationalAngle = (Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg);
            NewProjectile.transform.position = turretProjectileGen.transform.position;
            NewProjectile.transform.rotation = Quaternion.AngleAxis(rotationalAngle, Vector3.forward);
            NewProjectile.SetActive(true);
            NewProjectile.GetComponent<Rigidbody2D>().AddForce(directionVector.normalized * BulletForce);
        }
    }

    public void SetTurretSprite(bool input)
    {
        if (input)
        {
            GetComponent<SpriteRenderer>().sprite = ActiveTurret;
            laser.enabled = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = OfflineTurret;
            laser.enabled = false;
        }
    }
}
