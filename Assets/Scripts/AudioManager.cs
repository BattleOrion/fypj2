﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public enum SOUND_EFFECT
    {
        Menu,
        InGame,
        Shoot,
        P_Die,
        AI_Die,
    }

    public AudioSource MusicSource;
    public AudioSource SoundSource;
    public AudioClip MenuMusic;
    public AudioClip InGameMusic;
    public AudioClip Player_Shoot;
    public AudioClip Player_Death;
    public AudioClip AI_Death;

    private AudioClip currMusic;

    public static AudioManager instance = null;
    // Carry over the directory over to all scenes
    // Instead of creating a new one in every scene
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        currMusic = MenuMusic;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayMusic(SOUND_EFFECT music)
    {
        switch (music)
        {
            case SOUND_EFFECT.Menu:
                if (currMusic == MenuMusic)
                    return;
                MusicSource.clip = MenuMusic;
                currMusic = MenuMusic;
                break;
            case SOUND_EFFECT.InGame:
                if (currMusic == InGameMusic)
                    return;
                MusicSource.clip = InGameMusic;
                currMusic = InGameMusic;
                break;
        }
        MusicSource.Play();
    }

        public void PlaySound(SOUND_EFFECT sound)
    {
        switch (sound)
        {
            case SOUND_EFFECT.Shoot:
                SoundSource.clip = Player_Shoot;
                break;
            case SOUND_EFFECT.P_Die:
                SoundSource.clip = Player_Death;
                break;
            case SOUND_EFFECT.AI_Die:
                SoundSource.clip = AI_Death;
                break;
        }
        SoundSource.Play();
    }
}
