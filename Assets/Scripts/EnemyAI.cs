﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public Player player;
    public float rotSpeed, angularBuffer, maxAngularVelocity, moveSpeed, maxVelocity;
    public float minDist, MaxDist;
    public int index;

    public int CurrEnemyHP;
    public int MaxEnemyHP;

    private Rigidbody2D rb;
    private float targetAngle, currAngle, angleDiff;
    private float dist;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {       
        MoveToPlayer();
    }

    void MoveToPlayer()
    {
        dist = Vector3.Distance(player.transform.position, transform.position);
        if (dist < MaxDist && dist > minDist)
            ThrustForward(FacePlayer(), moveSpeed);
    }

    float GetPlayerDir()
    {
        Vector3 dir = player.transform.position - transform.position;
        dir.Normalize();
        targetAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;

        currAngle = rb.rotation;

        targetAngle = targetAngle + (360 - currAngle);
        targetAngle = targetAngle % 360;

        return targetAngle;
    }

    bool FacePlayer()
    {
        angleDiff = GetPlayerDir();
        if (angleDiff < angularBuffer || angleDiff > (360 - angularBuffer))
            return true;
        else if (angleDiff < 180)
            rb.AddTorque(rotSpeed);
        else
            rb.AddTorque(-rotSpeed);

        ClampRotation();
        return false;
    }

    void ThrustForward(bool facingTarget, float amount)
    {
        Vector2 force = transform.up * amount;
        if (!facingTarget)
            force *= 0.3f;
        rb.AddForce(force);
        ClampVelocity();
    }

    void ClampVelocity()
    {
        float x = Mathf.Clamp(rb.velocity.x, -maxVelocity, maxVelocity);
        float y = Mathf.Clamp(rb.velocity.y, -maxVelocity, maxVelocity);

        rb.velocity = new Vector2(x, y);
    }

    void ClampRotation()
    {
        float r = Mathf.Clamp(rb.angularVelocity, -maxAngularVelocity, maxAngularVelocity);

        rb.angularVelocity = r;
    }

    public void DealDamage(int damage)
    {
        CurrEnemyHP -= damage;
        if (CurrEnemyHP <= 0)
        {
            AudioManager.instance.PlaySound(AudioManager.SOUND_EFFECT.AI_Die);
            gameObject.SetActive(false);

            if (GetComponent<SceneSwitcher>() != null)     //if boss
            {
                GetComponent<SceneSwitcher>().SwitchScene(4);
            }
        }
    }
}
