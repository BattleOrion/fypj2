﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

    public static Grid instance = null;
    public Vector2 dimensions;
    public float gridSize = 1;
    public Vector2 offset;

    public List<Vector2> blockedTiles;

    public TileStatus placementTilePrefab;
    private TileStatus[,] m_Tiles;

    private Tower tower;
    private Vector2 towerOffset;

    public bool DisplayGrid;

    public int energyCount;
    public int energyUsable;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        SetUpGrid();
        tower = null;
    }

    void SetUpGrid()
    {
        if (placementTilePrefab != null)
        {
            // Create a container that will hold the tile
            var tilesParent = new GameObject("Container");
            tilesParent.transform.parent = transform;
            tilesParent.transform.localPosition = Vector3.zero;
            tilesParent.transform.localRotation = Quaternion.identity;
            m_Tiles = new TileStatus[(int)dimensions.x, (int)dimensions.y];

            for (int y = 0; y < dimensions.y; y++)
            {
                for (int x = 0; x < dimensions.x; x++)
                {
                    //Build Tile
                    TileStatus newTile = Instantiate(placementTilePrefab);
                    newTile.transform.parent = tilesParent.transform;
                    var position = new Vector3((x - 0.5f) * gridSize, (y - 0.5f) * gridSize, -0.1f);
                    newTile.transform.position = position;
                    //newTile.transform.localRotation = Quaternion.identity;
                    newTile.transform.localScale = new Vector3(gridSize, gridSize, 1);

                    newTile.SetIsTower(true);
                    newTile.SetState(TileStatus.TileState.empty);
                    newTile.SetType(TileStatus.TileTypeEnum.empty);
                    newTile.name = "(" + x.ToString() + "," + y.ToString() + ")";
                    newTile.SetGridPos(x, y);
                    m_Tiles[x,y] = newTile;

                    for (int i = 0; i < blockedTiles.Count; i++)
                    {
                        if (x == blockedTiles[i].x & y == blockedTiles[i].y)
                            newTile.SetState(TileStatus.TileState.occupied);
                    }

                }
            }

            tilesParent.transform.localRotation = transform.localRotation;
            tilesParent.transform.localPosition += new Vector3(0, -0.6f, 0);
        }
    }

    public bool Build(TileStatus tile)
    {
        if (tower == null)
            return false;

        Vector2 towerSize = tower.GetDimension();
        Vector2 offset = GetTowerToBuildOffset();

        //Check if build is allowed
        for (int i = 0; i < towerSize.x; ++i)
        {
            for (int j = 0; j < towerSize.y; ++j)
            {
                if (tile.GetGridPos().x + i - offset.x >= dimensions.x || tile.GetGridPos().x + i - offset.x < 0 || tile.GetGridPos().y + j - offset.y >= dimensions.y || tile.GetGridPos().y + j - offset.y < 0)
                    return false;
                if (m_Tiles[(int)tile.GetGridPos().x + i - (int)offset.x, (int)tile.GetGridPos().y + j - (int)offset.y].GetState() != TileStatus.TileState.empty)
                    return false;          
            }
        }

        //Build Allowed
        //Set Tiles to occupied
        for (int i = 0; i < towerSize.x; ++i)
        {
            for (int j = 0; j < towerSize.y; ++j)
            {
                m_Tiles[(int)tile.GetGridPos().x + i - (int)offset.x, (int)tile.GetGridPos().y + j - (int)offset.y].SetState(TileStatus.TileState.occupied);
                m_Tiles[(int)tile.GetGridPos().x + i - (int)offset.x, (int)tile.GetGridPos().y + j - (int)offset.y].SetType(tower.setType);
            }
        }

        //Build Tower
        Tower newTower = Instantiate(tower);
        newTower.towerData.SetFunctional();
        switch (tower.setType)
        {
            case TileStatus.TileTypeEnum.gun:
            {
                Destroy(newTower.transform.GetChild(1).gameObject);     //original container is kept for some reason so it needs to be removed
                newTower.GetComponent<BasicTurretScript>().SetTurretSprite(newTower.towerData.CheckFunctional());
                break;
            }
            case TileStatus.TileTypeEnum.rocket:
            {
                Destroy(newTower.transform.GetChild(1).gameObject);     //original container is kept for some reason so it needs to be removed
                break;
            }
            case TileStatus.TileTypeEnum.energy:
            {
                Destroy(newTower.transform.GetChild(0).gameObject);     //original container is kept for some reason so it needs to be removed
                break;
            }
            case TileStatus.TileTypeEnum.barrier:
            {
                Destroy(newTower.transform.GetChild(1).gameObject);     //original container is kept for some reason so it needs to be removed
                break;
            }
        }
        newTower.transform.parent = m_Tiles[(int)tile.GetGridPos().x - (int)offset.x, (int)tile.GetGridPos().y - (int)offset.y].transform;
        var towerOffset = new Vector3(0.5f * gridSize + tower.GetOffset().x, 0.5f * gridSize + tower.GetOffset().y, 0);
        newTower.transform.position = newTower.transform.parent.position + new Vector3(0.0f, 0.0f, -0.1f) - towerOffset;
        newTower.transform.localRotation = Quaternion.identity;
        newTower.SetAllTileToState(TileStatus.TileState.occupied);

        RecalculateEnergy();
        GetComponent<PlayerCollider>().ColliderCheck();
        return true;
    }

    public void Unbuild(TileStatus tile)
    {
        Vector2 parentGridPos = tile.transform.parent.parent.parent.gameObject.GetComponent<TileStatus>().GetGridPos();
        Vector2 towerSize = tower.GetDimension();
        
        tower.towerData.SetFunctional();

        tower.towerData.isFunctional = false;
        tower.GetComponent<CircleCollider2D>().enabled = false;

        //set grid tiles to be empty
        for (int i = 0; i < towerSize.x; ++i)
        {
            for (int j = 0; j < towerSize.y; ++j)
            {
                m_Tiles[(int)parentGridPos.x + i, (int)parentGridPos.y + j].SetState(TileStatus.TileState.empty);
                m_Tiles[(int)parentGridPos.x + i, (int)parentGridPos.y + j].SetType(TileStatus.TileTypeEnum.empty);
            }
        }

        if (tile.TileType == TileStatus.TileTypeEnum.energy)
        {
            EnergyScript removedTile = tile.GetComponentInParent<EnergyScript>();
            removedTile.SetOnline(false);
        }
        else if (tile.TileType == TileStatus.TileTypeEnum.barrier)
        {
            BarrierScript removedTile = tile.GetComponentInParent<BarrierScript>();
            removedTile.DisableBarrier();
        }
        else if (tile.TileType == TileStatus.TileTypeEnum.gun || tile.TileType == TileStatus.TileTypeEnum.rocket)
        {
            BasicTurretScript removedTurret = tile.GetComponentInParent<BasicTurretScript>();
            removedTurret.SetTurretSprite(false);
        }

        //detach tower from parent grid
        tile.transform.parent.parent.parent = null;

        RecalculateEnergy();
        GetComponent<PlayerCollider>().ColliderCheck();
    }

    public void SetTowerToBuild(Tower newTower)
    {
        tower = newTower;
    }

    public void SetTowerToBuildOffset(Vector2 offset)
    {
        towerOffset = offset;
    }

    public Vector2 GetTowerToBuildOffset()
    {
        return towerOffset;
    }

    public TileStatus[,] GetTiles()
    {
        return m_Tiles;
    }

    public Vector2 GetDimensions()
    {
        return dimensions;
    }

    public void RecalculateEnergy()
    {
        energyCount = energyUsable = 0;
        EnergyScript[] energyCalculator = GetComponentsInChildren<EnergyScript>();
        foreach (EnergyScript obj in energyCalculator)
        {
            if (obj.GetOnline())
            {
                energyCount += obj.energyValue;
            }
            else
            {
                if (obj.GetComponent<DataScript>().CheckFunctional())
                {
                    obj.SetOnline(true);
                    energyCount += obj.energyValue;
                }
            }
        }
        energyUsable = energyCount;

        Tower[] towerList = GetComponentsInChildren<Tower>();
        foreach (Tower obj in towerList)
        {
            if (obj.towerData.canBePowered())
            {
                if (energyUsable - obj.towerData.energyCost >= 0)
                {
                    energyUsable -= obj.towerData.energyCost;
                    obj.towerData.setPowered(true);
                }
                else
                {
                    obj.towerData.setPowered(false);
                }
            }
            else
            {
                obj.towerData.setPowered(true);
            }
        }
    }

#if UNITY_EDITOR
        // Draw grids in the editor
        void OnDrawGizmos()
    {
        if (DisplayGrid == true)
        {
            Color prevCol = Gizmos.color;
            Gizmos.color = Color.green;

            Matrix4x4 originalMatrix = Gizmos.matrix;
            Gizmos.matrix = transform.localToWorldMatrix;

            // Draw local space flattened cubes
            for (int y = 0; y < dimensions.y; y++)
            {
                for (int x = 0; x < dimensions.x; x++)
                {
                    var position = new Vector3(((x + 0.5f) * gridSize + offset.x) / transform.localScale.x, ((y + 0.5f) * gridSize + offset.y) / transform.localScale.y, 0);
                    Gizmos.DrawWireCube(position, new Vector3(gridSize / transform.localScale.x, gridSize / transform.localScale.y, 0));
                }
            }

            Gizmos.matrix = originalMatrix;
            Gizmos.color = prevCol;
        }
    }

    void OnValidate()
    {
        if (gridSize <= 0)
        {
            gridSize = 1;
        }
        
        if (dimensions.x <= 0 ||
            dimensions.y <= 0)
        {
            dimensions = new Vector2(1, 1);
        }
    }
#endif
}


