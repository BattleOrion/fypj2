﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public Vector2 dimensions;
    public float gridSize = 1;
    public Vector2 offset;

    public TileStatus placementTilePrefab;
    private TileStatus[,] m_Tiles;

    private bool CheckingIfBuildAble;

    private Vector2 gridDimension;
    private TileStatus[,] gridTiles;

    public TileStatus.TileTypeEnum setType;

    private Vector2 towerOffset;

    private TileStatus newHoverOverTile, CurrHoverOverTile, TempHoverOverTile, prevHoverOverTile;

    public DataScript towerData;

    void Awake()
    {
        SetUpGrid();
    }

    void Update()
    {
        if (CheckingIfBuildAble == true)
        {
            CheckIfBuildable();
        }
    }

    void SetUpGrid()
    {
        if (placementTilePrefab != null)
        {
            // Create a container that will hold the tiles
            var tilesParent = new GameObject("Container");
            tilesParent.transform.parent = transform;
            tilesParent.transform.localPosition = Vector3.zero;
            tilesParent.transform.localRotation = Quaternion.identity;
            m_Tiles = new TileStatus[(int)dimensions.x, (int)dimensions.y];

            for (int y = 0; y < dimensions.y; y++)
            {
                for (int x = 0; x < dimensions.x; x++)
                {
                    // Build Tile
                    TileStatus newTile = Instantiate(placementTilePrefab);
                    newTile.transform.parent = tilesParent.transform;
                    var position = new Vector3((x + 0.5f) * gridSize + offset.x, (y + 0.5f) * gridSize + offset.y, -0.1f);
                    newTile.transform.position = position + tilesParent.transform.position;
                    newTile.transform.localRotation = Quaternion.identity;
                    newTile.transform.localScale = new Vector3(gridSize, gridSize, 1);

                    if (setType == TileStatus.TileTypeEnum.barrier)
                    {
                        newTile.isBarrier = true;
                    }
                    else if (setType == TileStatus.TileTypeEnum.gun || setType == TileStatus.TileTypeEnum.rocket)
                    {
                        newTile.isTurret = true;
                    }

                    newTile.SetIsTower(false);
                    newTile.SetState(TileStatus.TileState.empty);
                    newTile.SetType(setType);
                    newTile.name = "(" + x.ToString() + "," + y.ToString() + ")";
                    newTile.SetGridPos(x, y);
                    m_Tiles[x, y] = newTile;
                }
            }
        }
    }

    public Vector2 GetDimension()
    {
        return dimensions;
    }

    public Vector2 GetOffset()
    {
        return offset;
    }

    public void SetAllTileToState(TileStatus.TileState state)
    {
        for (int y = 0; y < dimensions.y; y++)
        {
            for (int x = 0; x < dimensions.x; x++)
            {
                m_Tiles[x, y].SetState(state);
            }
        }
    }

    public void SetCheckingIfBuildAble(bool checking)
    {
        CheckingIfBuildAble = checking;

        if (checking == true)
        {
            for (int y = 0; y < dimensions.y; y++)
            {
                for (int x = 0; x < dimensions.x; x++)
                {
                    m_Tiles[x, y].SetIsCheckingSprite();
                }
            }
        }
        else
        {
            for (int y = 0; y < dimensions.y; y++)
            {
                for (int x = 0; x < dimensions.x; x++)
                {
                    m_Tiles[x, y].SetState(m_Tiles[x, y].GetState());
                }
            }
        }
    }

    private void CheckIfBuildable()
    {
        if (newHoverOverTile != null && CurrHoverOverTile != newHoverOverTile)
        {
            CurrHoverOverTile = newHoverOverTile;
            //Debug.Log("A: " + CurrHoverOverTile.name);
        }

        if (prevHoverOverTile != null && TempHoverOverTile != prevHoverOverTile)
        {
            TempHoverOverTile = prevHoverOverTile;
            //Debug.Log("B: " + prevHoverOverTile.name);
        }

        if (TempHoverOverTile != null && CurrHoverOverTile != null)
        {
            if (CurrHoverOverTile == TempHoverOverTile)
            {
                //Debug.Log("OUT");

                for (int i = 0; i < dimensions.x; ++i)
                {
                    for (int j = 0; j < dimensions.y; ++j)
                    {
                        m_Tiles[i, j].SetSprite("check");
                    }
                }
                
                CurrHoverOverTile = null;
                TempHoverOverTile = null;
                newHoverOverTile = null;
                prevHoverOverTile = null;
                return;
            }
        }

        if (CurrHoverOverTile != null)
        {
            for (int i = 0; i < dimensions.x; ++i)
            {
                for (int j = 0; j < dimensions.y; ++j)
                {
                    if ((int)CurrHoverOverTile.GetGridPos().x + i - (int)towerOffset.x >= gridDimension.x || (int)CurrHoverOverTile.GetGridPos().x + i - (int)towerOffset.x < 0 
                        || (int)CurrHoverOverTile.GetGridPos().y + j - (int)towerOffset.y >= gridDimension.y || (int)CurrHoverOverTile.GetGridPos().y + j - (int)towerOffset.y < 0)
                    {
                        m_Tiles[i, j].SetSprite("check");
                    }
                    else if (gridTiles[(int)CurrHoverOverTile.GetGridPos().x + i - (int)towerOffset.x, (int)CurrHoverOverTile.GetGridPos().y + j - (int)towerOffset.y].GetState() == TileStatus.TileState.occupied)
                    {
                        m_Tiles[i, j].SetSprite("occupied");
                    }
                    else if (gridTiles[(int)CurrHoverOverTile.GetGridPos().x + i - (int)towerOffset.x, (int)CurrHoverOverTile.GetGridPos().y + j - (int)towerOffset.y].GetState() == TileStatus.TileState.empty)
                    {
                        m_Tiles[i, j].SetSprite("empty");
                    }
                }
            }
        }

    }

    public void SetGridTiles(TileStatus[,] tiles)
    {
        gridTiles = tiles;
    }

    public void SetGridDimensions(Vector2 dimensions)
    {
        gridDimension = dimensions;
    }

    public void SetTowerToBuildOffset(Vector2 offset)
    {
        towerOffset = offset;
    }

    public Vector2 GetTowerToBuildOffset()
    {
        return towerOffset;
    }

    public void SetHoverOverTile(TileStatus tile)
    {
        newHoverOverTile = tile;
    }

    public TileStatus GetHoverOverTile()
    {
        return newHoverOverTile;
    }

    public void SetPrevHoverOverTile(TileStatus tile)
    {
        prevHoverOverTile = tile;
    }

    public TileStatus GetPrevHoverOverTile()
    {
        return prevHoverOverTile;
    }

    public void DealDamage(int damage)
    {
        towerData.TakeDamage(damage);
        if (setType == TileStatus.TileTypeEnum.barrier)
        {
            if (!towerData.CheckFunctional())
            {
                GetComponent<BarrierScript>().DisableBarrier();
                GetComponentInParent<PlayerCollider>().ColliderCheck();
            }
        }
        else if (setType == TileStatus.TileTypeEnum.energy)
        {
            if (!towerData.CheckFunctional())
            {
                GetComponent<EnergyScript>().SetOnline(false);
                Grid.instance.RecalculateEnergy();
                GetComponentInParent<PlayerCollider>().ColliderCheck();
            }
        }
        else if (setType == TileStatus.TileTypeEnum.gun || setType == TileStatus.TileTypeEnum.rocket)
        {
            if (!towerData.CheckFunctional())
            {
                GetComponent<BasicTurretScript>().SetTurretSprite(false);
                GetComponentInParent<PlayerCollider>().ColliderCheck();
            }
        }
    }

#if UNITY_EDITOR
    // Draw grids in the editor
    void OnDrawGizmos()
    {
        Color prevCol = Gizmos.color;
        Gizmos.color = Color.green;

        Matrix4x4 originalMatrix = Gizmos.matrix;
        Gizmos.matrix = transform.localToWorldMatrix;

        // Draw local space flattened cubes
        for (int y = 0; y < dimensions.y; y++)
        {
            for (int x = 0; x < dimensions.x; x++)
            {
                var position = new Vector3(((x + 0.5f) * gridSize + offset.x) / transform.localScale.x, ((y + 0.5f) * gridSize + offset.y) / transform.localScale.y, 0);
                Gizmos.DrawWireCube(position, new Vector3(gridSize / transform.localScale.x, gridSize / transform.localScale.y, 0));
            }
        }

        Gizmos.matrix = originalMatrix;
        Gizmos.color = prevCol;
    }

    void OnValidate()
    {
        if (gridSize <= 0)
        {
            gridSize = 1;
        }

        if (dimensions.x <= 0 ||
            dimensions.y <= 0)
        {
            dimensions = new Vector2(1, 1);
        }
    }
#endif
}
