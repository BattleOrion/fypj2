﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TileStatus : MonoBehaviour
{
    public enum TileState {empty, occupied};
    public Sprite empty, occupied, check, emptyHover, occupiedHover;

    public enum TileTypeEnum { empty, gun, rocket, energy, barrier };

    public TileTypeEnum TileType;

    private TileState currState;
    private SpriteRenderer rend;
    private Sprite prevSprite;

    //Used to remeber the position the mouse was from the centre of the gameobject
    //so that the object when following the mouse wont snap to the centre of the mouse
    private Vector3 mouseOffset;

    //If true means can be move moved and placed onto a grid
    //Should be false on main ship/grid and true on towers
    private bool isTower;

    private Vector2 gridPos;

    //For mouse single mouse up and down interactions
    private bool isFollowingMouse, released;

    private TileStatus hoverOverTile, prevHoverOverTile;

    private bool EditorMode;
    public bool isBarrier;
    public bool isTurret;

    void Awake()
    {
        rend = GetComponent<SpriteRenderer>();
        SetState(TileState.empty);
        released = true;
        EditorMode = true;
    }

    void OnMouseDown()
    {
        if (EditorMode == false)
            return;

        if (!isTower && isFollowingMouse == false)    //If tower
        {
            TileStatus[] gos = (TileStatus[])GameObject.FindObjectsOfType(typeof(TileStatus));
            foreach (TileStatus go in gos)
            {
                if (go.GetIsFollowMouse() == true)
                {
                    return;
                }
            }

            //Set current tower to be to the one that is built
            Grid.instance.SetTowerToBuild(GetComponentInParent<Tower>());
            Grid.instance.SetTowerToBuildOffset(GetGridPos());
            GetComponentInParent<Tower>().SetTowerToBuildOffset(GetGridPos());

            //Set the mouseOffset of the mouse from the center of object
            Vector3 localPosMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.parent.parent.position.z);
            Vector3 worldPosMouse = Camera.main.ScreenToWorldPoint(localPosMouse);
            mouseOffset = worldPosMouse - transform.parent.parent.position;

            //Disable collider so can interact with other gameobjects
            GetComponent<Collider2D>().enabled = false;
            GetComponentInParent<Collider2D>().enabled = false;
            
            if (currState == TileState.occupied)
            {
                GetComponentInParent<Tower>().SetAllTileToState(TileState.empty);
                GetComponentInParent<Grid>().Unbuild(this);
            }

            GetComponentInParent<Tower>().SetCheckingIfBuildAble(true);
            GetComponentInParent<Tower>().SetGridTiles(Grid.instance.GetTiles());
            GetComponentInParent<Tower>().SetGridDimensions(Grid.instance.GetDimensions());

            isFollowingMouse = true;
            released = false;

        }
        else if (isTower)               //If ship/grid
        {
            if (GetComponentInParent<Grid>().Build(this))   //if build successful
            {
                //find current gameobject and attached to mouse and remove it
                TileStatus[] gos = (TileStatus[])GameObject.FindObjectsOfType(typeof(TileStatus));
                foreach (TileStatus go in gos)
                {
                    if (go.GetIsFollowMouse() == true)
                    {
                        Destroy(go.transform.parent.parent.gameObject);
                    }
                }
                //Debug.Log("Build success");
            }
            //else
                //Debug.Log("Build failed");
        }
    }

    void OnMouseUp()
    {
        if (EditorMode == false)
            return;

        released = true;
    }

    void OnMouseEnter()
    {
        if (EditorMode == false)
            return;

        if (GetComponentInParent<Tower>() != null && GetComponentInParent<Grid>() != null)
        {
            hoverOverTile = this.transform.parent.parent.parent.GetComponent<TileStatus>();
            //Debug.Log("IN: " + hoverOverTile.name);
        }
        else
        {
            hoverOverTile = this;
            //Debug.Log("IN: " + hoverOverTile.name);
        }

        TileStatus[] gos = (TileStatus[])GameObject.FindObjectsOfType(typeof(TileStatus));
        foreach (TileStatus go in gos)
        {
            if (go.GetIsFollowMouse() == true)
            {
                go.GetComponentInParent<Tower>().SetHoverOverTile(hoverOverTile);
            }
        }
    }

    void OnMouseExit()
    {
        if (EditorMode == false)
            return;

        if (GetComponentInParent<Tower>() != null && GetComponentInParent<Grid>() != null)
        {
            prevHoverOverTile = this.transform.parent.parent.parent.GetComponent<TileStatus>();
            //Debug.Log("OUT: " + prevHoverOverTile.name);
        }
        else
        {
            prevHoverOverTile = this;
            //Debug.Log("OUT: " + prevHoverOverTile.name);
        }

        TileStatus[] gos = (TileStatus[])GameObject.FindObjectsOfType(typeof(TileStatus));
        foreach (TileStatus go in gos)
        {
            if (go.GetIsFollowMouse() == true)
            {
                go.GetComponentInParent<Tower>().SetPrevHoverOverTile(prevHoverOverTile);
            }
        }
    }

    private void Update()
    {
        if (EditorMode == false)
            return;

        if (isFollowingMouse)
        {
            //Object follow mouse with mouseOffset
            Vector3 localPosMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.parent.parent.position.z);
            Vector3 worldPosMouse = Camera.main.ScreenToWorldPoint(localPosMouse);
            worldPosMouse = worldPosMouse - mouseOffset;
            transform.parent.parent.position = worldPosMouse;

            //stop object from following mouse
            if (Input.GetMouseButtonDown(0) && released == true)
            {
                GetComponent<Collider2D>().enabled = true;
                GetComponentInParent<Collider2D>().enabled = true;

                GetComponentInParent<Tower>().SetCheckingIfBuildAble(false);

                Grid.instance.SetTowerToBuild(null);
                Grid.instance.SetTowerToBuildOffset(Vector2.zero);

                isFollowingMouse = false;                
            }           
        }
    }

    public void SetState(TileState newState)
    {
        currState = newState;
        switch (newState)
        {
            case TileState.empty:
                rend.sprite = empty;
                break;
            case TileState.occupied:
                rend.sprite = occupied;
                break;
        }
    }

    public void SetType(TileTypeEnum newType)
    {
        TileType = newType;
    }

    public TileState GetState()
    {
        return currState;
    }

    public void SetIsTower(bool tower)
    {
        isTower = tower;
    }

    public void SetGridPos(int x, int y)
    {
        gridPos = new Vector2(x, y);
    }

    public Vector2 GetGridPos()
    {
        return gridPos;
    }

    public bool GetIsFollowMouse()
    {
        if (isFollowingMouse)
            return true;
        else
            return false;
    }

    public void SetIsCheckingSprite()
    {
        rend.sprite = check;
    }

    public void SetSprite(string newSprite)
    {
        switch (newSprite)
        {
            case "empty":
                rend.sprite = emptyHover;
                break;
            case "occupied":
                rend.sprite = occupiedHover;
                break;
            case "check":
                rend.sprite = check;
                break;
        }
    }

    public void SetEditorMode(bool mode)
    {
        EditorMode = mode;
        rend.enabled = EditorMode;


        if (GetComponentInParent<Grid>() == null)
        {
            if (GetComponentInParent<CircleCollider2D>().gameObject.tag == "Player_Tower")
            {
                GetComponentInParent<CircleCollider2D>().enabled = false;
            }
            if (TileType == TileTypeEnum.barrier)
            {
                if (EditorMode == true)
                {
                    gameObject.GetComponentInParent<BarrierScript>().DisableBarrier();
                }
            }
        }
        else if (GetComponentInParent<Grid>() != null)
        {
            Grid Player = GetComponentInParent<Grid>();

            if (isBarrier)
            {
                BarrierScript[] barrierArray = Player.GetComponentsInChildren<BarrierScript>();
                foreach (BarrierScript go in barrierArray)
                {
                    CircleCollider2D temp = go.BarrierObj.GetComponent<CircleCollider2D>();
                    if (EditorMode == false)
                    {
                        if ((go.ReadyToCharge == true) && (go.GetComponent<Tower>().towerData.CheckFunctional()))
                        {
                            go.rechargeBool = true;
                            go.ReadyToCharge = false;
                        }
                        if (go.GetComponent<Tower>().towerData.CheckFunctional())
                        {
                            go.BarrierObj.GetComponent<CircleCollider2D>().enabled = true;
                        }
                    }
                    else
                    {
                        if ((go.ReadyToCharge == false) && (go.GetComponent<Tower>().towerData.CheckFunctional()))
                        {
                            go.ReadyToCharge = true;
                            go.rechargeBool = false;
                        }
                        go.BarrierObj.GetComponent<CircleCollider2D>().enabled = false;
                    }
                }
            }
            if (isTurret)
            {
                BasicTurretScript[] turretArray = Player.GetComponentsInChildren<BasicTurretScript>();
                foreach (BasicTurretScript go in turretArray)
                {
                    if (EditorMode == false)
                    {
                        if ((go.canFire == false) && (go.GetComponent<Tower>().towerData.CheckFunctional()))
                        {
                            go.canFire = true;
                        }
                    }
                    else
                    {
                        if ((go.canFire == true) && (go.GetComponent<Tower>().towerData.CheckFunctional()))
                        {
                            go.canFire = false;
                        }
                    }
                }
            }
        }
    }
}
