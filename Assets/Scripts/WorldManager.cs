﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldManager : MonoBehaviour
{
    public Player player;
    public GameObject sampleBG;

    //  0 | 3 | 6
    //  1 | 4 | 7
    //  2 | 5 | 8
    public GameObject[] section = new GameObject[9];
    public Asteriod Asteriod1;
    public EnemyAI[] Enemy = new EnemyAI[4];
    public GameObject SpaceStation;
    public EnemyAI bossPrefab;
    public Tower[] towerPrefab;
    public int minSectionEnemies, maxSectionEnemies, minSectionAsteriods, maxSectionAsteriods;
    public int escalationLevel, escalationFrequency, escalationMax; //escalationLevel is the current aditional difficlty, escalationFrequency is in seconds
    public int StationRarity;
    public int bossMinDist, bossMaxDist;

    private Vector3 sizeBG;
    private List<Asteriod> Asteriods = new List<Asteriod>();
    private List<EnemyAI> Enemies = new List<EnemyAI>();
    private List<GameObject> SpaceStations = new List<GameObject>();
    private List<Tower> Towers = new List<Tower>();
    private EnemyAI Boss;
    private float elaspedTime = 0;

    private Vector2 bossSpawnSection;
    private bool isBossSpawned;
    private Vector2[] sectionGridNum = new Vector2[9];

    public Text textRed;
    public Text textGreen;

    void Start()
    {
        //get size of section for reference later
        sizeBG = sampleBG.GetComponent<Renderer>().bounds.size;

        //  0 | 3 | 6
        //  1 | 4 | 7
        //  2 | 5 | 8

        //  -1, 1  |  0, 1 | 1, 1
        //  -1, 0  | 0, 0  | 1, 0
        //  -1, -1 | 0, -1 | 1, -1

        sectionGridNum[0] = new Vector2(-1, 1);
        sectionGridNum[1] = new Vector2(-1, 0);
        sectionGridNum[2] = new Vector2(-1, -1);
        sectionGridNum[3] = new Vector2(0, 1);
        sectionGridNum[4] = new Vector2(0, 0);
        sectionGridNum[5] = new Vector2(0, -1);
        sectionGridNum[6] = new Vector2(1, 1);
        sectionGridNum[7] = new Vector2(1, 0);
        sectionGridNum[8] = new Vector2(1, -1);

        //fill starting section with enemies and asteriods
        for (int i = 0; i < 8; ++i)
        {
            if (i >= 4)
                SpawnGameObject(i + 1);
            else
                SpawnGameObject(i);
        }

        //Start the game inside a station
        GameObject newStation = Instantiate(SpaceStation, section[4].transform);


        //Dont inherit parent scale
        newStation.transform.localScale = Asteriod1.transform.localScale / section[4].transform.localScale.x;
        //Randomise potion in section
        newStation.transform.position += new Vector3(Random.Range(-sizeBG.x * 0.2f, sizeBG.x * 0.2f), Random.Range(-sizeBG.y * 0.2f, sizeBG.y * 0.2f), -1);
        newStation.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
        SpaceStations.Add(newStation);

        Tower newTower = Instantiate(towerPrefab[0]);
        newTower.transform.position = new Vector3(newStation.transform.position.x, newStation.transform.position.y, newTower.transform.position.z);
        Towers.Add(newTower);

        Vector2 bossPos = new Vector2(Random.Range(bossMinDist, bossMaxDist + 1), Random.Range(bossMinDist, bossMaxDist + 1));
        if (Random.Range(0, 2) == 1)
            bossPos.x = -bossPos.x;
        if (Random.Range(0, 2) == 1)
            bossPos.y = -bossPos.y;
        bossSpawnSection = bossPos;
    }

    void Update()
    {
        //Continuiously raise the difficulty of the game after a certain time
        Escalation();
        //update section depending on player postion;
        ShiftBackground();
        //update enemies parent depending on their position
        UpdateObject();
        //Check if player is within a station's radius for it to edit itself
        InStation();

        //User interface
        GUI();
    }
    void ShiftBackground()
    {
        //player out of center section
        if (player.transform.position.y > section[4].transform.position.y + sizeBG.y * 0.5f)    //Up
        {
            ShiftUp();
        }
        else if (player.transform.position.y < section[4].transform.position.y - sizeBG.y * 0.5f)   //Down
        {
            ShiftDown();
        }

        if (player.transform.position.x > section[4].transform.position.x + sizeBG.x * 0.5f)        //Right
        {
            ShiftRight();
        }
        else if (player.transform.position.x < section[4].transform.position.x - sizeBG.x * 0.5f)       //Left
        {
            ShiftLeft();
        }

        //Debug.Log(sectionGridNum[0] + ", " + sectionGridNum[3] + ", " + sectionGridNum[6] + "\n" +
        //        sectionGridNum[1] + ", " + sectionGridNum[4] + ", " + sectionGridNum[7] + "\n" +
        //        sectionGridNum[2] + ", " + sectionGridNum[5] + ", " + sectionGridNum[8] + "\n");

        if (isBossSpawned == false)
        {
            for (int i = 0; i < 9; ++i)
            {
                if (sectionGridNum[i] == bossSpawnSection) //if sectiongrid num is same as boss spawn location, spawn the boss
                {
                    
                    EnemyAI newBoss = Instantiate(bossPrefab, section[i].transform);
                    //Dont inherit parent scale
                    newBoss.transform.localScale = Asteriod1.transform.localScale / section[i].transform.localScale.x;
                    //Randomise potion in section
                    newBoss.transform.position += new Vector3(Random.Range(-sizeBG.x * 0.2f, sizeBG.x * 0.2f), Random.Range(-sizeBG.y * 0.2f, sizeBG.y * 0.2f), -1);
                    newBoss.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
                    newBoss.player = player;
                    newBoss.GetComponentInChildren<EnemyTurretScript>().player = player;
                    newBoss.index = i;
                    Boss = newBoss;
                    isBossSpawned = true;
                }
            }
        }
    }

    void ShiftLeft()
    {
        //move right most section to be the new left most section
        section[6].transform.position += Vector3.left * (sizeBG.x * 3);
        section[7].transform.position += Vector3.left * (sizeBG.x * 3);
        section[8].transform.position += Vector3.left * (sizeBG.x * 3);

        //delete everything originally there
        DestroyAllChildren(section[6]);
        DestroyAllChildren(section[7]);
        DestroyAllChildren(section[8]);
        DestroyTower();

        //spawn new new things
        SpawnGameObject(6);
        SpawnGameObject(7);
        SpawnGameObject(8);

        //reset section ordering
        GameObject[] temp = new GameObject[9];
        temp[0] = section[6];
        temp[1] = section[7];
        temp[2] = section[8];

        temp[3] = section[0];
        temp[4] = section[1];
        temp[5] = section[2];

        temp[6] = section[3];
        temp[7] = section[4];
        temp[8] = section[5];

        section = temp;

        Vector2[] temp2 = new Vector2[9];

        temp2[0] = new Vector2(sectionGridNum[0].x - 1, sectionGridNum[0].y);
        temp2[1] = new Vector2(sectionGridNum[1].x - 1, sectionGridNum[1].y);
        temp2[2] = new Vector2(sectionGridNum[2].x - 1, sectionGridNum[2].y);

        temp2[3] = sectionGridNum[0];
        temp2[4] = sectionGridNum[1];
        temp2[5] = sectionGridNum[2];

        temp2[6] = sectionGridNum[3];
        temp2[7] = sectionGridNum[4];   
        temp2[8] = sectionGridNum[5];

        sectionGridNum = temp2;

        player.transform.parent = section[4].transform;
        UpdateEnemyIndex();
    }

    void ShiftRight()
    {
        //move right most section to be the new left most section
        section[0].transform.position += Vector3.right * (sizeBG.x * 3);
        section[1].transform.position += Vector3.right * (sizeBG.x * 3);
        section[2].transform.position += Vector3.right * (sizeBG.x * 3);

        //delete everything originally there
        DestroyAllChildren(section[0]);
        DestroyAllChildren(section[1]);
        DestroyAllChildren(section[2]);
        DestroyTower();

        //spawn new new things
        SpawnGameObject(0);
        SpawnGameObject(1);
        SpawnGameObject(2);

        //reset section ordering
        GameObject[] temp = new GameObject[9];
        temp[0] = section[3];
        temp[1] = section[4];
        temp[2] = section[5];

        temp[3] = section[6];
        temp[4] = section[7];
        temp[5] = section[8];

        temp[6] = section[0];
        temp[7] = section[1];
        temp[8] = section[2];

        section = temp;

        Vector2[] temp2 = new Vector2[9];

        temp2[0] = sectionGridNum[3];
        temp2[1] = sectionGridNum[4];
        temp2[2] = sectionGridNum[5];

        temp2[3] = sectionGridNum[6];
        temp2[4] = sectionGridNum[7];
        temp2[5] = sectionGridNum[8];

        temp2[6] = new Vector2(sectionGridNum[6].x + 1, sectionGridNum[6].y);
        temp2[7] = new Vector2(sectionGridNum[7].x + 1, sectionGridNum[7].y);
        temp2[8] = new Vector2(sectionGridNum[8].x + 1, sectionGridNum[8].y);
        
        sectionGridNum = temp2;

        player.transform.parent = section[4].transform;
        UpdateEnemyIndex();
    }

    void ShiftUp()
    {
        //move right most section to be the new left most section
        section[2].transform.position += Vector3.up * (sizeBG.y * 3);
        section[5].transform.position += Vector3.up * (sizeBG.y * 3);
        section[8].transform.position += Vector3.up * (sizeBG.y * 3);

        //delete everything originally there
        DestroyAllChildren(section[2]);
        DestroyAllChildren(section[5]);
        DestroyAllChildren(section[8]);
        DestroyTower();

        //spawn new new things
        SpawnGameObject(2);
        SpawnGameObject(5);
        SpawnGameObject(8);

        //reset section ordering
        GameObject[] temp = new GameObject[9];
        temp[0] = section[2];
        temp[1] = section[0];
        temp[2] = section[1];

        temp[3] = section[5];
        temp[4] = section[3];
        temp[5] = section[4];

        temp[6] = section[8];
        temp[7] = section[6];
        temp[8] = section[7];

        section = temp;

        Vector2[] temp2 = new Vector2[9];

        temp2[0] = new Vector2(sectionGridNum[0].x, sectionGridNum[0].y + 1);
        temp2[3] = new Vector2(sectionGridNum[3].x, sectionGridNum[3].y + 1);
        temp2[6] = new Vector2(sectionGridNum[6].x, sectionGridNum[6].y + 1);

        temp2[1] = sectionGridNum[0];
        temp2[4] = sectionGridNum[3];
        temp2[7] = sectionGridNum[6];

        temp2[2] = sectionGridNum[1];
        temp2[5] = sectionGridNum[4];
        temp2[8] = sectionGridNum[7];

        sectionGridNum = temp2;

        player.transform.parent = section[4].transform;
        UpdateEnemyIndex();
    }

    void ShiftDown()
    {
        //move right most section to be the new left most section
        section[0].transform.position += Vector3.down * (sizeBG.y * 3);
        section[3].transform.position += Vector3.down * (sizeBG.y * 3);
        section[6].transform.position += Vector3.down * (sizeBG.y * 3);

        //delete everything originally there
        DestroyAllChildren(section[0]);
        DestroyAllChildren(section[3]);
        DestroyAllChildren(section[6]);
        DestroyTower();

        //spawn new new things
        SpawnGameObject(0);
        SpawnGameObject(3);
        SpawnGameObject(6);

        //reset section ordering
        GameObject[] temp = new GameObject[9];
        temp[0] = section[1];
        temp[1] = section[2];
        temp[2] = section[0];

        temp[3] = section[4];
        temp[4] = section[5];
        temp[5] = section[3];

        temp[6] = section[7];
        temp[7] = section[8];
        temp[8] = section[6];

        section = temp;

        Vector2[] temp2 = new Vector2[9];

        temp2[0] = sectionGridNum[1];
        temp2[3] = sectionGridNum[4];
        temp2[6] = sectionGridNum[7];

        temp2[1] = sectionGridNum[2];
        temp2[4] = sectionGridNum[5];
        temp2[7] = sectionGridNum[8];

        temp2[2] = new Vector2(sectionGridNum[2].x, sectionGridNum[2].y - 1);
        temp2[5] = new Vector2(sectionGridNum[5].x, sectionGridNum[5].y - 1);
        temp2[8] = new Vector2(sectionGridNum[8].x, sectionGridNum[8].y - 1);

        sectionGridNum = temp2;

        player.transform.parent = section[4].transform;
        UpdateEnemyIndex();
    }

    void SpawnGameObject(int spawnInSection)
    {
        // Spawning asteriods //

        //Get number of asteriods to spawn
        int asteriodsToSpawn = Random.Range(minSectionAsteriods, maxSectionAsteriods + 1);      //max plus one cause unity Random.Range(int, int) max is exclusive
        for (int i = 0; i < asteriodsToSpawn; ++i)
        {
            Asteriod newAsteriod = Instantiate(Asteriod1, section[spawnInSection].transform);
            //Dont inherit parent scale
            newAsteriod.transform.localScale = Asteriod1.transform.localScale / section[spawnInSection].transform.localScale.x;
            //Randomise potion in section
            newAsteriod.transform.position += new Vector3(Random.Range(-sizeBG.x * 0.5f, sizeBG.x * 0.5f), Random.Range(-sizeBG.y * 0.5f, sizeBG.y * 0.5f), -1);
            newAsteriod.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0,360)));
            newAsteriod.index = spawnInSection;
            Asteriods.Add(newAsteriod);
        }
        
        // Spawning enemies //
        
        //Get number of enemies to spawn
        int EnemiesToSpawn = Random.Range(minSectionEnemies, maxSectionEnemies + 1);                //max plus one cause unity Random.Range(int, int) max is exclusive
        for (int i = 0; i < EnemiesToSpawn; ++i)
        {
            EnemyAI newEnemy = Instantiate(Enemy[Random.Range(0,4)], section[spawnInSection].transform);
            //Dont inherit parent scale
            newEnemy.transform.localScale /= sampleBG.transform.localScale.x;
            //Randomise potion in section
            newEnemy.transform.position += new Vector3(Random.Range(-sizeBG.x * 0.5f, sizeBG.x * 0.5f), Random.Range(-sizeBG.y * 0.5f, sizeBG.y * 0.5f), -1);
            newEnemy.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360)));
            newEnemy.index = spawnInSection;
            //set player for enemy
            newEnemy.player = player;
            Enemies.Add(newEnemy);
        }
        
        int rand = Random.Range(0, 100);    //random numbr between 0 - 99
        if (rand < StationRarity)
        {
            GameObject newStation = Instantiate(SpaceStation, section[spawnInSection].transform);
            //Dont inherit parent scale
            newStation.transform.localScale = Asteriod1.transform.localScale / section[spawnInSection].transform.localScale.x;
            //Randomise potion in section
            newStation.transform.position += new Vector3(Random.Range(-sizeBG.x * 0.5f, sizeBG.x * 0.5f), Random.Range(-sizeBG.y * 0.5f, sizeBG.y * 0.5f), -1);
            newStation.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360))); 
            SpaceStations.Add(newStation);

            //Tower newTower = Instantiate(towerPrefab[0]);
            Tower newTower = Instantiate(towerPrefab[Random.Range(0, towerPrefab.Length)]);
            newTower.transform.position = new Vector3(newStation.transform.position.x, newStation.transform.position.y, newTower.transform.position.z);
            Towers.Add(newTower);
        }

    }

    void DestroyAllChildren(GameObject obj)
    {
        //destroy children and remove from any list
        foreach (Transform child in obj.transform)
        {
            if (child.GetComponent<Asteriod>() != null) //if asteriod
                Asteriods.Remove(child.GetComponent<Asteriod>());
            else if (child.GetComponent<SpriteRenderer>().sprite == Enemy[0].GetComponent<SpriteRenderer>().sprite
                    || child.GetComponent<SpriteRenderer>().sprite == Enemy[1].GetComponent<SpriteRenderer>().sprite
                    || child.GetComponent<SpriteRenderer>().sprite == Enemy[2].GetComponent<SpriteRenderer>().sprite
                    || child.GetComponent<SpriteRenderer>().sprite == Enemy[3].GetComponent<SpriteRenderer>().sprite
                    )   //if enemy
                Enemies.Remove(child.GetComponent<EnemyAI>());
            else if (child.GetComponent<SpriteRenderer>().sprite == SpaceStation.GetComponent<SpriteRenderer>().sprite) //if station
                SpaceStations.Remove(child.gameObject);
            else if (child.GetComponent<SpriteRenderer>().sprite == Boss.GetComponent<SpriteRenderer>().sprite)     //if boss
                isBossSpawned = false;

            GameObject.Destroy(child.gameObject);
        }
    }

    void UpdateEnemyIndex()
    {
        for (int i = 0; i < 8; ++i)
        {
            foreach (Transform child in section[i].transform)
            {
                if (child.GetComponent<EnemyAI>() != null)
                    child.GetComponent<EnemyAI>().index = i;
            }
        }
        
    }

    void DestroyTower()
    {
        List<Tower> newTowers = new List<Tower>();
        foreach (Tower tower in Towers)
        {         
            if (tower == null)
            {
                Towers.Remove(tower);
                break;
            }
            float xDist = Mathf.Abs(tower.transform.position.x - player.transform.position.x);
            float yDist = Mathf.Abs(tower.transform.position.y - player.transform.position.y);
            if (tower.transform.parent != null)
            {
                Towers.Remove(tower);
            }
            else if (xDist  > sizeBG.x * 1.5 || yDist > sizeBG.y * 1.5)
            {
                GameObject.Destroy(tower.gameObject);
            }
            else
            {
                newTowers.Add(tower);
            }           
        }
        Towers = newTowers;
    }

    void UpdateObject()
    {
        //  0 | 3 | 6
        //  1 | 4 | 7
        //  2 | 5 | 8

        foreach (Asteriod asteriod in Asteriods)
        {
            if (asteriod.transform.localPosition.y > 0.5f) //Up
            {
                int mod = asteriod.index % 3;
                if (mod - 1 < 0)
                {
                    asteriod.index += 2;
                    asteriod.transform.parent = section[asteriod.index].transform;
                    asteriod.transform.localPosition = Vector3.zero;
                }
                else
                {
                    asteriod.index -= 1;
                    asteriod.transform.parent = section[asteriod.index].transform;
                }
            }
            else if (asteriod.transform.localPosition.y < -0.5f) // Down
            {
                int mod = asteriod.index % 3;
                if (mod + 1 > 2)
                {
                    asteriod.index -= 2;
                    asteriod.transform.parent = section[asteriod.index].transform;
                    asteriod.transform.localPosition = Vector3.zero;
                }
                else
                {
                    asteriod.index += 1;
                    asteriod.transform.parent = section[asteriod.index].transform;
                }
            }

            if (asteriod.transform.localPosition.x < -0.5f) //Left
            {
                int comparision = asteriod.index - 3;
                if (comparision < 0)
                {
                    asteriod.index += 6;
                    asteriod.transform.parent = section[asteriod.index].transform;
                    asteriod.transform.localPosition = Vector3.zero;
                }
                else
                {
                    asteriod.index -= 3;
                    asteriod.transform.parent = section[asteriod.index].transform;
                }
            }
            else if (asteriod.transform.localPosition.x > 0.5f) //Right
            {
                int comparision = asteriod.index + 3;
                if (comparision > 8)
                {
                    asteriod.index -= 6;
                    asteriod.transform.parent = section[asteriod.index].transform;
                    asteriod.transform.localPosition = Vector3.zero;
                }
                else
                {
                    asteriod.index += 3;
                    asteriod.transform.parent = section[asteriod.index].transform;
                }
            }
        }

        foreach (EnemyAI enemy in Enemies)
        {           
            if (enemy.transform.localPosition.y > 0.5f) //Up
            {
                int mod = enemy.index % 3;
                if (mod - 1 < 0)
                    enemy.index += 2;
                else
                    enemy.index -= 1;

                enemy.transform.parent = section[enemy.index].transform;
            }
            else if (enemy.transform.localPosition.y < -0.5f) // Down
            {
                int mod = enemy.index % 3;
                if (mod + 1 > 2)
                    enemy.index -= 2;
                else
                    enemy.index += 1;

                enemy.transform.parent = section[enemy.index].transform;
            }

            if (enemy.transform.localPosition.x < -0.5f) //Left
            {
                int comparision = enemy.index - 3;
                if (comparision < 0)
                    enemy.index += 6;
                else
                    enemy.index -= 3;

                enemy.transform.parent = section[enemy.index].transform;
            }
            else if (enemy.transform.localPosition.x > 0.5f) //Right
            {
                int comparision = enemy.index + 3;
                if (comparision > 8)
                    enemy.index -= 6;
                else
                    enemy.index += 3;

                enemy.transform.parent = section[enemy.index].transform;
            }
        }

        if (isBossSpawned == true)
        {
            //Debug.Log(Boss.transform.localPosition);
            if (Boss.transform.localPosition.y > 0.5f) //Up
            {
                int mod = Boss.index % 3;
                if (mod - 1 < 0)
                    Boss.index += 2;
                else
                    Boss.index -= 1;

                Boss.transform.parent = section[Boss.index].transform;
            }
            else if (Boss.transform.localPosition.y < -0.5f) // Down
            {
                int mod = Boss.index % 3;
                if (mod + 1 > 2)
                    Boss.index -= 2;
                else
                    Boss.index += 1;

                Boss.transform.parent = section[Boss.index].transform;
            }

            if (Boss.transform.localPosition.x < -0.5f) //Left
            {
                int comparision = Boss.index - 3;
                if (comparision < 0)
                    Boss.index += 6;
                else
                    Boss.index -= 3;

                Boss.transform.parent = section[Boss.index].transform;
            }
            else if (Boss.transform.localPosition.x > 0.5f) //Right
            {
                int comparision = Boss.index + 3;
                if (comparision > 8)
                    Boss.index -= 6;
                else
                    Boss.index += 3;

                Boss.transform.parent = section[Boss.index].transform;
            }
        }
    }

    void Escalation()
    {
        if (escalationLevel != escalationMax)
        {
            elaspedTime += Time.deltaTime;
            if (elaspedTime > escalationFrequency)
            {
                ++escalationLevel;
                ++minSectionAsteriods;
                maxSectionAsteriods += 2;
                ++minSectionEnemies;
                maxSectionEnemies += 2;
                elaspedTime = 0;
            }
        }
    }

    void InStation()
    {
        foreach (GameObject station in SpaceStations)
        {
            if (Vector3.Distance(station.transform.position, player.transform.position) < 14)   //check distance less than radius
            {
                player.canEdit = true;  //allow edit mode
                break;
            }
            else
            {
                player.canEdit = false;
            }
        }
    }

    void GUI()
    {
        textRed.text = "Escalation Lvl: " + escalationLevel + "\n";
        float distFromBoss = (int)Vector2.Distance(player.transform.position, bossSpawnSection * sizeBG.x);

        if (distFromBoss < 100)
        {
            distFromBoss = Random.Range(100, 1000);
            textRed.text += "Boss in " + distFromBoss.ToString() + "m \n";
            textRed.text += "Danger Close! \n";
        }
        else
        {
            textRed.text += "Boss in " + distFromBoss.ToString() + "m \n";
        }

        textGreen.text = "HP: " + player.Hp + "\n";
        float shortestDist = float.MaxValue;
        float newDist;
        if (SpaceStations.Count != 0)
        {
            foreach (GameObject station in SpaceStations)
            {
                newDist = Vector3.Distance(player.transform.position, station.transform.position);
                if (newDist < shortestDist)
                    shortestDist = newDist;
            }
            textGreen.text += "Station in " + (int)shortestDist + "m";
        }
        else
        {
            textGreen.text += "Station in Unknown";
        }
    }
}
