﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggingScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnMouseDrag()
    {
        Vector3 newPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1);
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(newPos);
        worldPos.z = 0;
        gameObject.transform.position = worldPos;
    }

    public void Speak()
    {
        Debug.Log("Working");
    }

    public void OnMouseEnter()
    {
        Debug.Log("Working enter");
    }
}
