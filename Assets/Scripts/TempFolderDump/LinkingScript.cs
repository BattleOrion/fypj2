﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkingScript : MonoBehaviour {

    public class LocationData
    {
        public float LocationX;
        public float LocationY;
    }

    BoxCollider2D TileSizeData;
    public LocationData[][] ArrayData;
    public float SizeX;
    public float SizeY;



    // Use this for initialization
    void Start () {
        TileSizeData = gameObject.GetComponent<BoxCollider2D>();
        SizeX = (int)TileSizeData.size.x;
        SizeY = (int)TileSizeData.size.y;
        ArrayData = new LocationData[(int)TileSizeData.size.x][];
        for (int j = 0; TileSizeData.size.x > j; j++)
        {
            ArrayData[j] = new LocationData[(int)SizeY];
            for (int i = 0; TileSizeData.size.y > i; i++)
            {
                ArrayData[j][i] = new LocationData();
            }
        }

        for (int j = 0; TileSizeData.size.x > j; j++)
        {
            for (int i = 0; TileSizeData.size.y > i; i++)
            {
                ArrayData[j][i].LocationX = SizeX / 4 * j;
                ArrayData[j][i].LocationY = SizeY / 4 * i;
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

}
