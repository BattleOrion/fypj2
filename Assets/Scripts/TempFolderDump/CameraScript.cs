﻿using UnityEngine;
using System.Collections;

public class CameraScript: MonoBehaviour
{

    //Init Var
    public Transform target;
    public float CameraSpeed;
    public float CameraZoom;
    public GameObject CameraBoundary;

    private float XConstraintMin, XConstraintMax, YConstraintMin, YConstraintMax;

    private Camera myCam;

    // Use this for initialization
    void Start()
    {
        //Get Components
        myCam = GetComponent<Camera>();
        //Debug
        //if (XConstraintMin >= XConstraintMax || YConstraintMin >= YConstraintMax)
        //    Debug.Log("Camera constraint values inappopriate, please check.");
    }

    // Update is called once per frame
    void Update()
    {
        myCam.orthographicSize = (Screen.height / 100f) / CameraZoom;

        if (target) //If exist
        {
            //Move camera to target position using linear interpolation
            transform.position = Vector3.Lerp(transform.position, target.position, CameraSpeed);

            ////Also constraint camera if its position is more than or equal values specified
            transform.position = new Vector3(
                                                transform.position.x,
                                                transform.position.y,
                                                -10
                                            );
        }
    }
}