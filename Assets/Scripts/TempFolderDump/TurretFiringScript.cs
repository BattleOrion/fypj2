﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretFiringScript : MonoBehaviour {

    public GameObject BaseProjectilePrefab;
    public Vector3 directionVector;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            Vector3 newPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1);
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(newPos);
            worldPos.z = 0;
            directionVector = (worldPos - gameObject.transform.position).normalized;
            FireProjectile();
        }
	}

    public void FireProjectile()
    {
        GameObject NewProjectile;
        NewProjectile = Instantiate(BaseProjectilePrefab);
        NewProjectile.transform.position = transform.position;

        NewProjectile.GetComponent<Rigidbody2D>().AddForce(directionVector * 100.0f);

        // get component kys to add velocity throught function

        float rotationalAngle = (Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg);
        NewProjectile.transform.rotation = Quaternion.AngleAxis(rotationalAngle, Vector3.forward);

        //Vector2 CurrentVelocity = NewProjectile.GetComponent<Rigidbody2D>().velocity;
        //float rotationalAngle = (Mathf.Atan2(CurrentVelocity.y, CurrentVelocity.x) * Mathf.Rad2Deg);
        //Debug.Log(rotationalAngle);
        //NewProjectile.transform.rotation = Quaternion.AngleAxis(rotationalAngle + 90, Vector3.forward);

        //NewProjectile.GetComponent<Rigidbody2D>().MoveRotation(rotationalAngle);
    }
}
