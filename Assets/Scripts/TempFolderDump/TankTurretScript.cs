﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankTurretScript : MonoBehaviour
{
    [SerializeField]
    private GameObject turretBarrel, turretProjectilePrefab, turretProjectileGen, turretBarrelBody, turretAngleLimit_1, turretAngleLimit_2;


    public float firerateCooldown, turretFirerate;
    private Vector2 worldpos;
    public float BulletForce;

    public float turretAngleMin, turretAngleMax;

    public float lastAngle;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        LookAtTarget();

        firerateCooldown -= Time.deltaTime;
        if (firerateCooldown <= 0)
        {
            firerateCooldown = turretFirerate;
            FireProjectile();
        }
    }

    private void LookAtTarget()
    {
        worldpos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));

        float AngleRad = Mathf.Atan2(worldpos.y - turretBarrel.transform.position.y, worldpos.x - turretBarrel.transform.position.x);
        float AngleDeg = Mathf.Rad2Deg * AngleRad;

        if (AngleDeg > 0.0f)
        {

        }
        float LerpAngle = Mathf.LerpAngle(lastAngle, AngleDeg, Time.deltaTime);

        // If outside of range, rotate body until it is
        turretBarrel.GetComponent<Rigidbody2D>().MoveRotation(LerpAngle);


        // else if within range, rotate gun until it reach or can rotate both



        //if ((AngleDeg > 20.0f) || (AngleDeg < -20.0f))
        //{
        //    if (AngleDeg > 20.0f)
        //    {
        //        turretBarrelBody.GetComponent<Rigidbody2D>().MoveRotation(AngleDeg - 20.0f);
        //    }
        //    else if (AngleDeg < -20.0f)
        //    {
        //        turretBarrelBody.GetComponent<Rigidbody2D>().MoveRotation(20.0f + AngleDeg);
        //    }
        //    //Mathf.Clamp(AngleDeg, -20.0f, 20.0f);
        //}
        //else if ((AngleDeg < 20.0f) && (AngleDeg > turretAngleMax)) 
        //{
        //    turretBarrelBody.GetComponent<Rigidbody2D>().MoveRotation(Mathf.Lerp(AngleDeg,AngleDeg - 10.0f,Time.deltaTime));
        //}

        //if ((AngleDeg < turretAngleMin) || (AngleDeg > turretAngleMax))
        //{
        //    turretBarrel.GetComponent<Rigidbody2D>().MoveRotation(AngleDeg + 10);

        //}

        //else if ((AngleDeg < turretAngleMin) || (AngleDeg > turretAngleMax))
        //{
        //    turretBarrel.GetComponent<Rigidbody2D>().MoveRotation(AngleDeg + 10);

        //}
        //turretBarrel.GetComponent<Rigidbody2D>().MoveRotation(AngleDeg);

        lastAngle = AngleDeg;
        Debug.Log(AngleDeg);
    }

    private void FireProjectile()
    {
        GameObject NewProjectile;
        NewProjectile = Instantiate(turretProjectilePrefab);
        NewProjectile.transform.position = turretProjectileGen.transform.position;
        Vector2 directionVector = Vector2.zero;
        directionVector.Set(worldpos.x - turretBarrel.transform.position.x, worldpos.y - turretBarrel.transform.position.y);

        NewProjectile.GetComponent<Rigidbody2D>().AddForce(directionVector.normalized * BulletForce);

        float rotationalAngle = (Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg);
        NewProjectile.transform.rotation = Quaternion.AngleAxis(rotationalAngle, Vector3.forward);
    }
}
