﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class ObjectPoolItem
    {
        public GameObject storageObject;
        public int amountToPool;
        public GameObject objectToPool;
        public bool shouldExpand;
    }

    public static ObjectPooler SharedInstance;

    public Vector2 minCoord, maxCoord;


    public List<ObjectPoolItem> itemsToPool;
    public List<GameObject> pooledObjects;

    void Awake()
    {
        SharedInstance = this;
    }

    // Use this for initialization
    void Start()
    {
        pooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.amountToPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.objectToPool,item.storageObject.transform);
                obj.SetActive(false);
                pooledObjects.Add(obj);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject GetPooledObject(string tag)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].tag == tag)
            {
                return pooledObjects[i];
            }
        }
        foreach (ObjectPoolItem item in itemsToPool)
        {
            if (item.objectToPool.tag == tag)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objectToPool, item.storageObject.transform);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }

    public int GetActiveTag(string tagName)
    {
        int activeCount = 0;

        foreach (GameObject item in pooledObjects)
        {
            if (tagName == item.tag)
            {
                if (item.activeInHierarchy == true)
                {
                    activeCount++;
                }
            }
        }

        return activeCount;
    }

    public bool CheckWithinBounds(Vector2 inputCoord)
    {
        if (((minCoord.x < inputCoord.x) && (minCoord.y < inputCoord.y)) && 
            ((inputCoord.x < maxCoord.x) && (inputCoord.y < maxCoord.y)))
        {
            return true;
        }
        return false;
    }

    public Vector2 GetMinVector()
    {
        return minCoord;
    }

    public Vector2 GetMaxVector()
    {
        return maxCoord;
    }
}
