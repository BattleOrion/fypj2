﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool canEdit;
    bool EditorMode;
    public int Hp;

    // Start is called before the first frame update
    void Start()
    {
        EditorMode = false;
        SetEditorMode(EditorMode);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space") && canEdit == true)
        {
            EditorMode = !EditorMode;
            SetEditorMode(EditorMode);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GetComponent<SceneSwitcher>().SwitchScene(0);
        }
    }

    private void SetEditorMode(bool mode)
    {
        TileStatus[] gos = (TileStatus[])GameObject.FindObjectsOfType(typeof(TileStatus));
        foreach (TileStatus go in gos)
        {
            go.GetComponentInChildren<TileStatus>().SetEditorMode(mode);
        }

        GetComponent<PlayerMovement>().enabled = !mode;

        if (mode == true)
        {
            Destroy(GetComponent<Rigidbody2D>());
        }
        else
        {
            gameObject.AddComponent<Rigidbody2D>();
            GetComponent<Rigidbody2D>().gravityScale = 0;
            GetComponent<Rigidbody2D>().drag = GetComponent<PlayerMovement>().linearDrag;
            GetComponent<Rigidbody2D>().angularDrag = GetComponent<PlayerMovement>().angularDrag;
            GetComponent<PlayerMovement>().SetRigidBody(GetComponent<Rigidbody2D>());

        }

    }

    public void TakeDamage(int Dmg)
    {
        Debug.Log("Taking Damage");
        Hp -= Dmg;
        if (Hp <= 0)
            GetComponent<SceneSwitcher>().SwitchScene(3);
    }
}
